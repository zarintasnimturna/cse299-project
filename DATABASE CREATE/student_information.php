<?php

$link = mysqli_connect("localhost", "root", "", "nsu_system");
 
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 

$sql = "CREATE TABLE student_information(
    id int(255) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    student_id int(10),
    student_type VARCHAR(255) ,
    first_name VARCHAR(255) ,
    last_name VARCHAR(255) ,
    gender VARCHAR(255) ,
    birth_date VARCHAR(255) ,
    email VARCHAR(255) ,
    phone int(11),
    enroll VARCHAR(255) ,
    present_address VARCHAR(255) ,
    permanent_address VARCHAR(255) ,
    zip_code int(9) ,
    city VARCHAR(255) ,
    district VARCHAR(255) ,
    blood_group VARCHAR(50) ,
    father_name VARCHAR(255) ,
    mother_name VARCHAR(255) ,
    gurdian_phone1 int(11),
    gurdian_phone2 int(11),
    parent_address VARCHAR(255),
    parent_city VARCHAR(255) ,
    parent_district VARCHAR(255) ,
    parent_email VARCHAR(255) 

   
)";
if(mysqli_query($link, $sql)){
    echo "Table created successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
mysqli_close($link);

?>