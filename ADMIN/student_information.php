<?php include('server_student_information.php') ;
    if(isset($_GET['update'])){
        $id = $_GET['update'];

        $update = true;

        $rec= mysqli_query($db , "select * from student_information where id=$id");
        $record = mysqli_fetch_array($rec); 

        
        $student_type = $record['student_type'];
        $student_id = $record['student_id'];
        $first_name = $record['first_name'];
        $last_name = $record['last_name'];
        $gender = $record['gender'];
        $email = $record['email'];
        $phone = $record['phone'];
        $enroll = $record['enroll'];
        $present_address = $record['present_address'];
        $permanent_address = $record['permanent_address'];
        $zip_code = $record['zip_code'];
        $city = $record['city'];
        $district = $record['district'];
        $blood_group = $record['blood_group'];
        $father_name = $record['father_name'];
        $mother_name = $record['mother_name'];
        $gurdian_phone1 = $record['gurdian_phone1'];
        $gurdian_phone2 = $record['gurdian_phone2'];
        $parent_address = $record['parent_address'];
        $parent_city = $record['parent_city'];
        $parent_district = $record['parent_district'];
        $parent_email = $record['parent_email'];
        $id = $record['id'];
    }

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="form.css">
    <title>Register New Student</title>



    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css.map">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css.map">
    <link rel="stylesheet" href="../css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="../css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../css/bootstrap-reboot.rtl.min.css.map">
    <link rel="stylesheet" href="../css/bootstrap-utilities.css">
    <link rel="stylesheet" href="../css/bootstrap-utilities.css.map">
    <link rel="stylesheet" href="../css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="../css/bootstrap-utilities.min.css.map">
    <link rel="stylesheet" href="../css/bootstrap-utilities.rtl.css">
    <link rel="stylesheet" href="../css/bootstrap-utilities.rtl.css.map">
    <link rel="stylesheet" href="../css/bootstrap-utilities.rtl.min.css">
    <link rel="stylesheet" href="../css/bootstrap-utilities.rtl.min.css.map">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/bootstrap.css.map">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css.map">
    <link rel="stylesheet" href="../css/bootstrap.rtl.css">
    <link rel="stylesheet" href="../css/bootstrap.rtl.css.map">
    <link rel="stylesheet" href="../css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="../css/bootstrap.rtl.min.css.map">
</head>

<body>
     <!--nav bar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <img src="admin.jpg" alt="" height="100px">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link" aria-current="page" href="admin_home.html" style="margin: 20px;">Home</a>
              <a class="nav-link" href="student_information.php" style="margin: 20px;">Registration Form</a>
            </div>
          </div>

          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>

          <a href="admin_login.php" class="btn btn-info btn-lg" style="margin: 10px;">
            <span class="glyphicon glyphicon-log-out"></span> Log out
          </a>

        </div>
        
      </nav>

<div>
    <p>

    </p>
</div>

    
   <!--form-->


   
   <?php if(isset($_SESSION['msg'])):?>

<div class="msg">
<?php

    echo $_SESSION['msg'];
    unset($_SESSION['msg']);

?>
</div>

<?php endif ?>

   <form class="h-100 h-custom gradient-custom-2" method="post" action="server_student_information.php">

   <input type="hidden" name="id" value="<?php echo $id; ?>">
  
   <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12">
          <div class="card card-registration card-registration-2" style="border-radius: 15px;">
            <div class="card-body p-0">
              <div class="row g-0">
                <div class="col-lg-6">
                  <div class="p-5">
                    <h3 class="fw-normal mb-5" style="color: #4835d4;">Student Infomation</h3>
  
                    <div class="mb-4 pb-2">
                      <select class="select" style="width: 150px; height: 50px;" name="student_type" value="<?php echo $student_type; ?>" required>
                        <option disabled = "disables" selected="selected">Student Type</option>
                        <option>Undergraduate</option>
                        <option>Graduate</option>
                        
                      </select>
                    </div>

                    <div class="mb-4 pb-2">
                      <div class="form-outline">
                        <input type="text" id="form3Examplev4" name="student_id" value="<?php echo $student_id; ?>" class="form-control form-control-lg" style="width: 240px;" required>
                        <label class="form-label" for="form3Examplev4" >Student ID</label>
                      </div>
                    </div>

  
                    <div class="row">
                      <div class="col-md-6 mb-4 pb-2">
  
                        <div class="form-outline">
                          <input type="text" id="form3Examplev2" name="first_name"  value="<?php echo $first_name; ?>" class="form-control form-control-lg" required >
                          <label class="form-label" for="form3Examplev2">First name</label>
                        </div>
  
                      </div>
                      <div class="col-md-6 mb-4 pb-2">
  
                        <div class="form-outline">
                          <input type="text" id="form3Examplev3" class="form-control form-control-lg" name="last_name" value="<?php echo $last_name; ?>" required>
                          <label class="form-label" for="form3Examplev3">Last name</label>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <select class="select" style="width: 150px; height: 50px; margin-bottom: 40px;" name="gender" value="<?php echo $gender; ?>" required>
                          <option disabled = "disables" selected="selected">Gender</option>
                          <option>Male</option>
                          <option>Female</option>
                          <option>Other</option>
                        </select>
                      </div>

                      </div>

                    <div class="mb-4 pb-2">
                      <div class="form-outline">
                        <input type="text" id="form3Examplev4" class="form-control form-control-lg" name="email" value="<?php echo $email; ?>" style="margin-top: 20px;"required>
                        <label class="form-label" for="form3Examplev4">Email</label>
                      </div>
                    </div>

                    <div class="mb-4 pb-2">
                      <div class="form-outline">
                        <input type="text" id="form3Examplev4" class="form-control form-control-lg" name="phone" value="<?php echo $phone; ?>" required>
                        <label class="form-label" for="form3Examplev4">Contact Info</label>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <select class="select" style="width: 350px; height: 50px; margin-bottom: 40px;" name="enroll" value="<?php echo $enroll; ?>" required>
                          <option disabled = "disables" selected="selected">Enroll</option>
                          <option>a</option>
                          <option>b</option>
                          <option>c</option>
                        </select>
                      </div>
                      </div>

                    <div class="mb-4 pb-2">
                      <div class="form-outline">
                        <input type="text" id="form3Examplev4" class="form-control form-control-lg" name="present_address" value="<?php echo $present_address; ?>" required>
                        <label class="form-label" for="form3Examplev4">Present Address</label>
                      </div>
                    </div>

                    <div class="mb-4 pb-2">
                      <div class="form-outline">
                        <input type="text" id="form3Examplev4" class="form-control form-control-lg" name="permanent_address" value="<?php echo $permanent_address; ?>">
                        <label class="form-label" for="form3Examplev4">Permanent Address</label>
                      </div>
                    </div>

                    <div class="form-outline form-white">
                      <input type="text" id="form3Examplea4" class="form-control form-control-lg" style="width: 140px;" name="zip_code" value="<?php echo $zip_code; ?>" required>
                      <label class="form-label" for="form3Examplea4">Zip Code</label>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
  
                        <select class="select" style="width: 150px; height: 50px; margin: 10px;" name="city" value="<?php echo $city; ?>" required>
                          <option disabled = "disables" selected="selected">City</option>
                          <option>Dhaka</option>
                          <option>Chittagong</option>
                          <option>Khulna</option>
                        </select>
                      </div>

                      <div class="col-md-6">
                        <select class="select" style="width: 150px; height: 50px; margin: 10px;" name="district" value="<?php echo $district; ?>" required>
                          <option disabled = "disables" selected="selected">District</option>
                          <option>Two</option>
                          <option>Three</option>
                          <option>Four</option>
                        </select>
                      </div>

                      <div class="col-md-6">
                        <select class="select" name="blood_group" value="<?php echo $blood_group; ?>" style="width: 150px; height: 50px; margin: 10px;" required>
                          <option disabled = "disables" selected="selected">Blood Group</option>
                          <option>A+</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B-</option>
                          <option>O+</option>
                          <option>O-</option>
                          <option>AB+</option>
                          <option>AB-</option>
                        </select>
                      </div>

                    </div>
                  </div>
                </div>


                <div class="col-lg-6 bg-indigo text-white">
                  <div class="p-5">
                    <h3 class="fw-normal mb-5">Gurdian Infomation</h3>

                    <div class="row">
                      <div class="col-md-6 mb-4 pb-2">
  
                        <div class="form-outline">
                          <input type="text" id="form3Examplev2" class="form-control form-control-lg" name="father_name" value="<?php echo $father_name; ?>" required>
                          <label class="form-label" for="form3Examplev2">Father's Name</label>
                        </div>
  
                      </div>
                      <div class="col-md-6 mb-4 pb-2">
  
                        <div class="form-outline">
                          <input type="text" id="form3Examplev3" class="form-control form-control-lg" name="mother_name" value="<?php echo $mother_name; ?>" required>
                          <label class="form-label" for="form3Examplev3">Mother's Name</label>
                        </div>
  
                      </div>
                    </div>
  
                    <div class="mb-4 pb-2">
                      <div class="form-outline form-white">
                        <input type="text" id="form3Examplea2" class="form-control form-control-lg" name="gurdian_phone1" value="<?php echo $gurdian_phone1; ?>" required> 
                        <label class="form-label" for="form3Examplea2">Gurdian Contact (1)</label>
                      </div>
                    </div>
  
                    <div class="mb-4 pb-2">
                      <div class="form-outline form-white">
                        <input type="text" id="form3Examplea3" class="form-control form-control-lg" name="gurdian_phone2" value="<?php echo $gurdian_phone2; ?>">
                        <label class="form-label" for="form3Examplea3">Gurdian Contact (2)</label>
                      </div>
                    </div>
  
                    <div class="mb-4 pb-2">
                      <div class="form-outline form-white">
                        <input type="text" id="form3Examplea6" class="form-control form-control-lg" name="parent_address" value="<?php echo $parent_address; ?>" required>
                        <label class="form-label" for="form3Examplea6">Address</label>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
  
                        <select class="select" style="width: 150px; height: 50px; margin-bottom: 50px;" name="parent_city" value="<?php echo $parent_city; ?>" required>
                          <option disabled = "disables" selected="selected">City</option>
                          <option>Dhaka</option>
                          <option>Chittagong</option>
                          <option>Khulna</option>
                        </select>
                      </div>

                      <div class="col-md-6">
                        <select class="select" style="width: 150px; height: 50px; margin-bottom: 50px;" name="parent_district" value="<?php echo $parent_district; ?>" required>
                          <option disabled = "disables" selected="selected">District</option>
                          <option>Two</option>
                          <option>Three</option>
                          <option>Four</option>
                        </select>
                      </div>
                    </div>
  
                    <div class="mb-4">
                      <div class="form-outline form-white">
                        <input type="text" id="form3Examplea9" class="form-control form-control-lg" name="parent_email" value="<?php echo $parent_email; ?>">
                        <label class="form-label" for="form3Examplea9">Email Address if any</label>
                      </div>
                    </div>
  
                    <!--<button type="button" class="btn btn-light btn-lg" data-mdb-ripple-color="dark">Register</button>
-->
        <div class="input-group" >
      <?php if($update == false): ?>
        <button type="submit" class="btn btn-light btn-lg" name="save">Register</button>

      <?php endif ?>  
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      </form>





  <script src="../js/bootstrap.bundle.js"></script>
  <script src="../js/bootstrap.bundle.js.map"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js.map"></script>
  <script src="../js/bootstrap.esm.js"></script>
  <script src="../js/bootstrap.esm.js.map"></script>
  <script src="../js/bootstrap.esm.min.js"></script>
  <script src="../js/bootstrap.esm.min.js.map"></script>
  <script src="../js/bootstrap.js"></script>
  <script src="../js/bootstrap.js.map"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/bootstrap.min.js.map"></script>


</body>
</html>